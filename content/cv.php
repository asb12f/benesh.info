<div class="row" style="padding-left: 1em;">
	<h1>Curriculum Vitae</h1>
	<h2>Andrew S. Benesh, MFTi</h2>
</div>
<div class="row" style="padding-left: 1em;">
	<h3>Address</h2>
		<p>	<a href="http://www.fsu.edu/" target=_blank>Florida State University</a><br />
			Department of Family &amp; Child Sciences<br />
			228 Sandels Building<br />
			Tallahassee, FL 32306
		</p>
		<h3>Email</h3>
		<p>	<a href="mailto:asb12f@my.fsu.edu">asb12f@my.fsu.edu</a>
		</p>
	<h3>Education</h1>
		<ul>
			<li><a href="http://www.chs.fsu.edu/Departments/Family-Child-Sciences/Doctoral-Programs-in-Family-Child-Sciences/Marriage-and-Family-Therapy" target=_blank>PhD Marriage &amp; Family Therapy</a>, Florida State University, in progress
			<ul>
				<li><a href="http://education.fsu.edu/degrees-and-programs/measurement-and-statistics-graduate" target=_blank>Graduate Certificate in Measurement &amp; Statistics</a>, Florida State University, 2015.</li>
			</ul></li>
			<li><a href="https://medicine.mercer.edu/admissions/mft/" target=_blank>M.S Family Therapy</a>, Mercer University School of Medicine, 2012.</li>
			<li><a href="http://www.gcsu.edu/psychology/index.htm" target=_blank>B.S. Psychology</a>, Georgia College &amp; State University, 2008.</li>
		</ul>
	<h3>Publications &amp; Presentations</h3>
		<h4>Journal Articles</h4>
			<p class="reference"><strong>Benesh, A. S.</strong>, &amp; Cui, M. (<span>2016</span>) 
				Foster parent training programs for foster youth: A content review.<em>Child & Family Social Work</em>. doi:10.1111/cfs.12265. [<a href='http://onlinelibrary.wiley.com/doi/10.1111/cfs.12265/abstract' target=_blank>Link</a>]
			</p>
			<p class="reference"><strong>Benesh, A. S.</strong>, &amp; Cooley, M. E. (<span><em>in preparation</em></span>) 
				The effects of treatment foster care on adolescent delinquency: A meta-analysis. 
			</p>
			<p class="reference"><strong>Benesh, A. S.</strong>, &amp; Cui, M. (<span><em>in review</em></span>) 
				Effectiveness of Foster parent training programs for foster youth. 
			</p>
		<h4>Book Chapters</h4>
			<p class="reference">McWey, L., <strong>Benesh, A. S.</strong>, &amp; Wojciak, A. S. (2015). 
				Families with Children in Foster Care: Clinical Considerations and Interventions. 
				In S. Browning &amp; K. Pasley (Eds), <span><em>Contemporary Families: Translating Research Into Practice</em></span> 
				(pp. 70–87). New York, NY: Routledge. [<a href='https://books.google.com/books?hl=en&lr=&id=DifLCQAAQBAJ&oi=fnd&pg=PA70&dq=benesh+foster+care&ots=KWRtRYilZV&sig=UYNuLwNs_t_nkAtJHitLC4unxgU#v=onepage&q=benesh%20foster%20care&f=false' target=_blank>Link</a>]</p>
		<h4>Encyclopedia Articles</h4>
			<p class="reference"><strong>Benesh, A. S.</strong>, Cho, S., &amp; Cui, M. (<span><em>in press</em></span>). 
				Adolescent Behavior Disorders. In J. Carson and S. Dermer (Eds.), 
				<span><em>The SAGE Encyclopedia of Marriage, Family, and Couples Counseling</em></span>. 
				Thousand Oaks, CA: SAGE Publications, Inc. [<a href='https://us.sagepub.com/en-us/nam/the-sage-encyclopedia-of-marriage-family-and-couples-counseling/book243467' target=_blank>Link</a>]</p>
		<h4>Posters</h4>
			<p class="reference"><strong>Benesh, A. S.</strong>,&amp; Cooley, M. E. (2016, June). 
				<span><em>Is Multidimensional Treatment Foster Care Effective? A Review of the Evidence</em></span>, 
				24th Annual Colloquium of the American Professional Society on the Abuse of Children, New Orleans, LA. [<a href='./content/posters/APSAC_2016_TxFC.pdf' target=_blank>Link</a>]</p>
			<p class="reference">Cooley, M. E.,&amp; <strong>Benesh, A. S.</strong> (2016, June). 
				<span><em>Foster Child Health, Behaviors, and Relationship with Caregiver: Does Child-Caregiver Relationship Matter?</em></span>, 
				24th Annual Colloquium of the American Professional Society on the Abuse of Children, New Orleans, LA. [<a href='./content/posters/APSAC_2016_HealthBx.pdf' target=_blank>Link</a>]</p>
			<p class="reference"><strong>Benesh, A. S.</strong>, Murphy, K. M., &amp; Farineau, H. M. (2014, June). 
				<span><em>Social Support as a Protective Factor in Adolescent Pregnancy for Youths in Foster Care</em></span>, 
				36th Annual Meeting and Open Conference of the American Family Therapy Academy, Athens, GA. [<a href='./content/posters/AFTA_2014_SSPregnancyFC.pdf' target=_blank>Link</a>]</p>
			<p  class="reference">Murphy, K. M., &amp; <strong>Benesh, A. S.</strong> (2014, May). 
				<span><em>Social Support as a Moderator for Adolescent Maternal Depression and Child’s Externalizing Behaviors</em></span>, 
				19th National Conference on Child Abuse and Neglect, New Orleans, LA. [<a href='./content/posters/NCCAN_2014_SSMxDepression.pdf'  target=_blank>Link</a>]</p>
			<p  class="reference"><strong>Benesh, A. S.</strong>, &amp; Meyer, A. S. (2012, September). 
				<span><em>Patterns in Relational Treatments for Preschoolers</em></span>, 
				American Association of Marriage and Family Therapy Annual Conference, Charlotte, NC. [<a href='./content/posters/AAMFT_2012_RelationalTxPreschools.pdf' target=_blank>Link</a>]</p>
		<h4>Presentations</h2>
			<p  class="reference"><strong>Benesh, A. S.</strong> (2014, February). 
				<span><em>Gender Identity in Family Therapy</em></span>, 
				Guest Lecture for FSU Course FAD6930 Diversity in Marriage &amp; Family Therapy, Florida State University, Tallahassee, FL.</p></li>
			<p  class="reference"><strong>Benesh, A. S.</strong> (2013, September). 
				<span><em>Adolescent Psychosocial Development</em></span>, 
				Guest Lecture for FSU Course FAD3220 Individual and Family Lifespan Development, Florida State University, Tallahassee, FL.</p></li>
			<p  class="reference"><strong>Benesh, A. S.</strong> (2012, September). 
				<span><em>Building Relationships</em></span>, 
				Guest Lecture for FSU Course FAD2230 Individual and Family Lifespan Development, Florida State University, Tallahassee, FL.</p></li>
			<p  class="reference"><strong>Benesh, A. S.</strong> (2012, March). 
				<span><em>Families in Middle and Later Life</em></span>, 
				Guest Lecture for FSU Course FAD2230 Individual and Family Lifespan Development, Florida State University, Tallahassee, FL.</p></li>
			<p  class="reference"><strong>Benesh, A. S.</strong>, Cox, D., Cummings, C., Meyer, A. S., &amp; Templeton, B. (2011, October). 
				<span><em>Is Research a Dirty Word</em></span>, 
				Guest Lecture at Georgia Association of Marriage and Family Therapists Middle GA Chapter Meeting, Macon, GA.</p>
	<h3>Employment</h3>
		<ul>
			<li>Psychiatric Social Worker, <span><em>Tallahassee Memorial Hospital Behavioral Health Center, Tallahassee, FL</em></span>, 2015–present.</li>
			<li>Family Therapist, <span><em>FSU Center for Couple &amp; Family Therapy, Tallahassee, FL</em></span>, 2012–present.</li>
			<li>Instructor, <span><em>FSU</em></span>, 2014–present
				<ul>
					<li><a href="http://www.benesh.info/CHD4615" target=_blank>CHD 4615 Family and Child Public Policy</a></li>
					<li><a href="http://www.benesh.info/CHD4630" target=_blank>CHD 4630 Methods of Studying Families and Children</a></li>
				</ul></li>
			<li>Teaching Assistant, <span><em>FSU</em></span>, 2012–present
				<ul>
					<li>FAD 2230 Family Relationships</li>
					<li>FAD 3220 Individual and Family Lifespan Development</li>
					<li>FAD 3343 Contexts of Adult Development &amp; Aging</li>
					<li>FAD 3432 Stress and Resilience in Individuals and Families</li>
					<li>FAD 4630 Methods of Studying Families and Children</li>
					<li>FAD 4936 Contexts of Adolescent Development</li>
					<li>CHD 5695 Applied Research in Human Sciences</li>
				</ul></li>
			<li>Child &amp; Family Therapist Intern, <span><em>RiverEdge Behavioral Health Child &amp; Adolescent Crisis Stabilization Unit</em></span>, 2012.</li>
			<li>Medical Family Therapist Intern, <span><em>Medical Center of Central Georgia</em></span>, 2012.</li>
			<li>In-Home Family Therapist, <span><em>Dynamic Interventions, Inc.</em></span>, 2011–2012.</li>
			<li>Juvenile Detention Counselor, <span><em>Macon Youth Detention Center, GA Dept. Juvenile Justice</em></span>, 2008–2012.</li>
		</ul>
	<h3>Professional Affiliations, Licenses, &amp; Service</h3>
		<ul>
			<li>Marriage &amp; Family Therapist Registered Intern, Florida, # IMT2260</li>
			<li>Board Member &amp; Assistant Camp Director, <a href='http://www.camptobelong-ga.org/' target=_blank>Camp to Belong Georgia</a>, 2014 – present</li>
			<li>Family &amp; Child Sciences Representative, Graduate Student Advisory Council, 2014 – 2015</li>
			<li><a href='http://www.apsac.org/' target=_blank>APSAC</a>, <span><em>Member</em></span>, 2016–present</li>
			<li><a href='http://www.aamft.org/' target=_blank>AAMFT</a>, <span><em>Student Member</em></span>, 2010–present</li>
			<li><a href='http://www.ifta-familytherapy.org/' target=_blank>IFTA</a>, <span><em>Student Member</em></span>, 2012–present</li>
			<li><a href='https://www.ncfr.org/' target=_blank>NCFR</a> Conference Proposal Reviewer, 2014</li>
			<li>Mu Phi Theta, <span><em>Secretary</em></span>, 2011–2012</li>
		</ul>
	<h3>Volunteer Work</h3>
		<ul>
			<li>First Aid / CPR / AED Instructor, <span><em>American Red Cross of Capital Area Florida</em></span>, 2015 - present</li>
			<li>Disaster Action Team, <span><em>American Red Cross of Capital Area Florida</em></span>, 2015 - present</li>
			<li>Behavioral Health Camp Counselor, <span><em>Camp to Belong Georgia</em></span>, 2013 - present</li>
			<li>Mentor, <span><em><a href='http://uwbb.org/readingpals/' target=_blank>United Way Big Bend ReadingPALS</a></em></span>, 2013 – 2014</li>
		</ul>
	<h3>Awards &amp; Honors</h3>
		<ul>
			<li>Carolyn X. Hundall Memorial Scholarship, 2015</li>
			<li>FSU College of Human Sciences Fellowship, 2012–2014</li>
			<li>Dean’s List, 2008</li>
		</ul>
</div>