<div class="row" style="padding-left: 1em;">
	<h1>Research</h1>
</div>
<div class="row" style="padding: 1em;" id="ccft">
	<h2>Program of Research <hr></h2>
	<h3>Foster Parent Training &amp; Parent&ndash;Child Relationships</h3>
	<img src='./images/fosterparent.jpg' alt='Parent Child Relationships' class='img-responsive center-block hidden-xs' />	
	<p style="padding-top:1em;">	
		My primary area of research focuses on programs and interventions for foster parents, with an emphasis on foster child outcomes and
		improving relationships between foster parents, foster children, and birth families.
		Around <a href="http://www.acf.hhs.gov/programs/cb/research-data-technology/statistics-research/afcars">400,000 youth</a> are placed in foster, kinship, or group homes each year, and both State and Federal laws mandate training and supervision of foster parents,
		yet <a href="http://www.sciencedirect.com/science/article/pii/S0190740913003228">little research</a> has examined whether the currently mandated foster parent training programs have any benefits for foster youth.
		Projects include development of novel foster parent training programs, evaluating the role of foster parent&ndash;foster child relationships on foster child outcomes, 
		and developing valid measures of foster parent, foster child, and birth family relationships.
	</p>
	<h3>Child Welfare &amp; Juvenile Justice Outcomes</h3>
	<img src='./images/homeless.jpg' alt='Homeless Youth' class='img-responsive center-block hidden-xs' />
	<p style="padding-top:1em;">	
		My second area of research focuses on outcomes for youth and families served by the child welfare &amp; juvenile justice systems,
		and factors that contribute to positive or negative outcomes. Past research has found these youth to be at elevated risk for adverse outcomes 
		(homelessness, pregnancy, adult incarceration, etc.) and reduced likelihood of positive outcomes 
		(college acceptance &amp; graduation, employment, marriage, etc.). My research in this area emphasizes relational variables that influence 
		long&ndash; and short&ndash;term developmental outcomes for these youth.
	</p>
	<h2>Research Practices<hr></h2>
		<h3>Open Science</h3>
		<p style="padding-top:1em;">	
			One of the problems confronting modern science is the failure of researchers to communicate with one another about their research, and 
			to make their data and methods accessible public and replicable. I believe that one way to help combat this is to work from an
			<a href="http://centerforopenscience.org/">Open Science Framework</a>. As part of this effort I  make manuscripts,
			analyses, data, and code from my academic and personal projects available through my 
			<a href="https://bitbucket.org/asb12f/">Bitbucket</a> and <a href="https://github.com/asb12f">Github</a> accounts whenever possible,
			and prefer to use open-source technologies for analysis and writing, such as <a href="http://www.latex-project.org/">LaTex</a>, 
			<a href="https://www.r-project.org/">R</a>, and <a href="http://www.scipy.org/">Python</a>.
		</p>
		<h3>Collaboration</h3>
		<p>			
			In addition to providing greater transparency, these tools allow me to collaborate with researchers from anywhere at any time. 
			I'm always looking for new projects and collaboration opportunities, and encourage you to contact me if you're interested in
			working together.
		</p>
		<h3>Consultation &amp; Training</h3>
		<p>
			In addition to formal collaboration, I am available to for consulting and training on research design and statistical
			analysis using open source tools and methods. 
		</p>
</div>