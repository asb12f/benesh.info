		<div class="row" style="padding-left: 1em;">
			<h1 style="margin-bottom: 0;">Andrew S. Benesh, MFTi</h1>
			<h2 style="padding-top: 0; margin-top:0;"><small>Child and Adolescent Family Therapist</small></h2>
		</div>
		<div class="row hidden-xs" style="height: 500px;">
			<?php require_once('./content/carousel.php'); ?>
		</div>
		<div class="row" style="padding: 1em;">
			<h2>Welcome!</h2>
				<p>	I am a Family Therapy Intern currently working towards full licensure in the State of Florida.
					I specialize in work with children, adolescents, and their families, and also provide services for individual adults and couples. 
					I'm trained and experienced in treatment for families experiencing a wide range of problems, including trauma, abuse, 
					delinquency and behavioral problems, adoption and foster care, depression and anxiety, self-injury, functional disabilities (including autism and developmental disabilities),
					sexual identity and orientation, and divorce.
					I provide treatment using a collaborative systems-based approach that integrates evidence-based interventions with 
					the unique needs and experiences of each family.
				</p>
				<p>	In addition to my clinical practice I am PhD candidate at Florida State University, where I conduct research and teach
					in the College of Human Sciences. I serve as a Board Member for <a href="http://camptobelong-ga.org/">Camp to Belong Georgia</a>, a non-profit that works to re-unite
					siblings separated by foster care through week long summer camps, sibling connection events, and policy advocacy. I also serve 
					with the <a href="http://www.redcross.org/fl/tallahassee">Capital Area Chapter of the American Red Cross</a>, where I work as an Instructor and Disaster Action Team Member.
					I also dabble in web development, programming, photography, and public policy advocacy.
				</p>
			<h2>Background</h2>
				<p>	I hold an MS in Family Therapy from the Mercer University School of Medicine, and 
					am currently completing my PhD in Family Therapy at Florida State University.
					I've worked in secure juvenile justice facilities, residential crisis stabilization units, intensive in-home therapy programs,
					schools for youth with emotional and behavioral disturbances, and outpatient clinics.  
				</p>
					
				<p>	Along the way I've developed a unique set of therapeutic skills for supporting families in crisis,
					connecting with youth who've experienced trauma, loss, psychiatric disturbances, and functional
					disabilities.  I've become skilled at working within systems of care and collaborating with professionals
					from health, education, and social welfare disciplines.  
				</p>
		</div>