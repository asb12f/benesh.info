<div id="myCarousel" class="carousel" data-ride="carousel" data-interval="3000" >
  
	<!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox" style="background-color: black; padding:1em; height: 500px;">
  
    <div class="item active">
      <img class="img-responsive center-block" src="./images/family1.jpg" style="max-height: 500px; " alt="Family">
    </div>
	
    <!--
	Removed by request of Vancouver Public Library
	<div class="item">
      <img class="img-responsive center-block" src="./images/teen1.jpg" style="max-height: 500px;" alt="Teen">
    </div>
	-->

    <div class="item">
      <img class="img-responsive center-block" src="./images/family2.jpg" style="max-height: 500px;" alt="Family">
    </div>
	
	<div class="item">
      <img class="img-responsive center-block" src="./images/fosterparent.jpg" style="max-height: 500px;" alt="Foster Families">
    </div>

	<div class="item">
      <img class="img-responsive center-block" src="./images/family2.jpg" style="max-height: 500px;" alt="Family">
    </div>
	
    <div class="item">
      <img class="img-responsive center-block" src="./images/parentchild.jpg" style="max-height: 500px;" alt="Parent Child Relationships">
    </div>

    <div class="item">
      <img class="img-responsive center-block" src="./images/teen2.jpg" style="max-height: 500px;" alt="Teen">
    </div>
  </div>
 <script type="text/javascript">
        $('#myCarousel').on('slid', '', function () {
            if ($('.carousel-inner .item:last').hasClass('active')) {
                $(this).carousel('pause');
            }
        })
</script>
</div>