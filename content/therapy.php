<div class="row" style="padding-left: 1em;">
	<h1>Therapy &amp; Treatment Services</h1>
</div>
<div class="row" style="padding: 1em;" id="ccft">
	<h2>FSU Center for Couple and Family Therapy (CCFT)</h2>
	<img src='./images/ccft.jpg' alt='Center for Couple and Family Therapy' class='img-responsive center-block' />
	<h3>Overview</h3>
		<p>	The FSU CCFT is an outpatient clinic located on the FSU campus, and operated by FSU's doctoral program in Marriage &amp; Family Therapy.
			The CCFT provides high quality treatment to individuals, couples, and families in the Tallahassee and North Florida communities.			
			All therapists are current doctoral students, and hold a minimum of a Master's in MFT or a related clinical field.
			Services are provided to clients without discrimination on the basis of race, age, ethnicity, 
			socioeconomic status, disability, gender, health status, religion, national origin, sexual orientation, 
			gender identity or relationship status. 
			All therapy sessions are supervised by FSU faculty holding AAMFT Approved Supervisor status.
		</p>
	<h3>Services</h3>
		<p> CCFT therapists specialize in relationship education, premarital counselling, couple therapy, work &amp; school related stress, 
			parenting therapy &amp; training, step-parenting &amp; blended families therapy, 
			communication &amp; conflict resolution skill building, divorce &amp; child custody issues, grief &amp; loss, child behavior issues, 
			&amp; mental health treatment. 
		</p>
	<h3>Hours &amp; Availability</h3>
		<p>	The FSU CCFT is open Monday&ndash;Thursday, from 9:30AM&ndash;8:00PM; however individual therapist schedules vary.
			Walk-ins are not currently accepted, but intake appointments can typically be scheduled within 24&ndash;48 hours.
		</p>
	<h3>Fees</h3>
		<p>	FSU students pay a flat fee, and all community residents pay an amount based upon your family income.
			Intake sessions are &#36;10. We are currently unable to accept insurance.
		</p>
	<h3>Location &amp; Parking</h3>
		<p>	The FSU CCFT is located at 540 W. Jefferson Street, Tallahassee, FL, 32301. Parking is provided for clients in the rear of the building.
		</p>
		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item"
					src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3439.8523027631663!2d-84.29030039999998!3d30.440288599999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88ecf572da3b6a39%3A0x8dca7d862802e968!2s540+W+Jefferson+St%2C+Tallahassee%2C+FL+32301!5e0!3m2!1sen!2sus!4v1440028956276" 
					></iframe>
		</div>
		<h3>Contact Us</h3>
		<p>To schedule an intake appointment, please call <a href="tel:850-644-1588">850-644-1588</a>, or use the form <a href="http://ccft.fsu.edu/Contact-CCFT/Email-us">here</a>.</p>
<!-- Form doesn't quite work yet. Waiting for update.


		<form method="post" action="http://ccft.fsu.edu/Contact-CCFT/Email-us/content/action">
			<legend>To schedule an intake appointment, please call <a href="tel:850-644-1588">850-644-1588</a>, or use the form below.</legend>
			<div class="form-group form-inline">
				<label class="control-label col-md-2" for="ContentObjectAttribute_ezstring_data_text_1893809">Your Name</label>
				<input class="box" type="text" size="70" name="ContentObjectAttribute_ezstring_data_text_1893809" value="" />
			</div>
			<div class="form-group form-inline">
				<label class="control-label col-md-2" for="ContentObjectAttribute_data_text_1893812">Your Email</label>
				<input class="box" type="text" size="70" name="ContentObjectAttribute_data_text_1893812" value="" />
			</div>
			<div class="form-group form-inline">
				<label class="control-label col-md-2" for="ContentObjectAttribute_ezstring_data_text_1893810">Subject</label>
				<input class="box" type="text" size="70" name="ContentObjectAttribute_ezstring_data_text_1893810" value="" />
			</div>
			<div class="form-group form-inline">
				<label class="control-label col-md-2" for="ContentObjectAttribute_data_text_1893811">Message</label>
				<textarea class="box" name="ContentObjectAttribute_data_text_1893811" cols="70" rows="10"></textarea> 
			</div>
			<div class="form-group form-inline">
				<button type="submit" class="btn btn-success" name="ActionCollectInformation" value="Send form">Submit</button>
				<input type="hidden" name="ContentNodeID" value="872188" />
				<input type="hidden" name="ContentObjectID" value="221708" />
				<input type="hidden" name="ViewMode" value="full" />
			</div>
		</form>
-->
</div>