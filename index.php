<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel='shortcut icon' href='./images/bamboo.ico' type='image/x-icon'/ >
	<title>Andrew Benesh, MFT</title>
	<!-- SEO Meta Tags --->
	<meta name="description" value="Andrew Benesh is a registered Marriage and Family Therapy Intern in the State of Florida, and currently 
		completing his PhD in Marriage and Family Therapy at FSU."> 
	<meta name="author" value="Andrew S. Benesh"> 

	<!-- Social Media Tags --->
	<meta property="og:title" value="Andrew Benesh, MFT">
	<meta property="og:image" value="./images/family1.jpg">
	<meta property="og:description" content="Andrew Benesh is a registered Marriage and Family Therapy Intern in the State of Florida, and currently 
		completing his PhD in Marriage and Family Therapy at FSU.">

	<!-- Adding Bootstrap -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


	<!-- Adding Google Fonts-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link href="./css/custom.css" rel="stylesheet">
</head>
<body>

<?php require_once('./global/nav.php'); ?>
<div class="container-fluid" style="margin-top: 60px;">
	<div class="row">
		<div class="col-md-8 col-md-offset-2" style="border-right: 1px solid black; border-left: 1px solid black;">
<?php
$page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_SPECIAL_CHARS);

if (empty($page) || $page == 'home'){
	require_once('./content/home.php');
} elseif ($page == 'research') {
	require_once('./content/research.php');
} elseif ($page == 'therapy') {
	require_once('./content/therapy.php');
} elseif ($page == 'service') {
	require_once('./content/service.php');
} elseif ($page == 'teaching') {
	require_once('./content/teaching.php');
} elseif ($page == 'cv') {
	require_once('./content/cv.php');
} else {
	echo '<div class="row">';
	echo '<div  style="text-align: center;" class="col-md-8 col-md-offset-2">';
	echo '<h1>Uh oh! It looks like this page isn&#39;t ready yet!</h1>';
	echo '<h3>Perhaps you should go back to the <a href="?page=home">Home Page</a>.</h3>';
	echo '</div>';
}
?>
		<?php require_once('./global/footer.php'); ?>
	</div>
</div>

</div>
  	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script>
		$('.carousel').carousel({
			pause: "false"
		});
	</script>
</body>
</html>