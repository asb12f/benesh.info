<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KL4WL7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KL4WL7');</script>
<!-- End Google Tag Manager -->

<div class="navbar navbar-inverse navbar-fixed navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand active">Andrew Benesh, MFTi</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="?page=home">Home</a></li>
				<li class="active dropdown">
					<a href="index.html#" class="dropdown-toggle" data-toggle="dropdown">Teaching<b class="caret"></b></a>
					<ul class="dropdown-menu">
					<li class="hide"><a href="?page=teaching" >Teaching Philosophy</a></li>
						<li><a href="../CHD4615" >CHD4615 Family and Child Public Policy</a></li>
						<li><a href="../CHD4630" >CHD4630 Methods of Studying Children and Families</a></li>
					</ul>
				</li>
				<li class="active"><a href="?page=therapy">Therapy</a></li>
				<li class="active"><a href="?page=research">Research</a></li>
				<li class="active hide"><a href="?page=service">Service</a></li>
				<li class="active"><a href="?page=cv">CV</a></li>
				<li class="active hide"><a href="#">Blog</a></li>
			</ul>
		</div>
	</div>
</div> 
